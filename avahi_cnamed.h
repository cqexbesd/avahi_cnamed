#ifndef AVAHI_CNAMED_H
#define AVAHI_CNAMED_H

#include <sys/queue.h>

#include <avahi-client/publish.h>
#include <avahi-common/simple-watch.h>

struct cname_record {
	SLIST_ENTRY(cname_record) next;

	const char *name;
	AvahiEntryGroup *entry_group;
};

struct context {
	SLIST_HEAD(, cname_record) cnames;

	AvahiClient *client;
	AvahiSimplePoll *poll;

	const char *file;
	int ec;

	volatile bool keep_running;
};

#ifndef SLIST_FOREACH_SAFE
#define SLIST_FOREACH_SAFE(var, head, field, tvar)				\
	for ((var) = SLIST_FIRST((head));							\
			(var) && ((tvar) = SLIST_NEXT((var), field), 1);	\
			(var) = (tvar))
#endif

#endif
