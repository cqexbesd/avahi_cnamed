# avahi_cnamed

Add aliases to Avahi to be published. Use this to advertise multiple names via mDNS.

Makefile assumes BSD make (aka fmake under some Linuxes). If you are running Linux you will
need libbsd installed as well.
