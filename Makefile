PROG=avahi_cnamed

MAN=

CSTD=gnu11
WARNS=7
NO_WERROR=true

LDADD=-lavahi-client -lavahi-common

.ifdef DEBUG
CFLAGS=-O0 -g -pipe
.endif

BSD != uname -s | grep BSD || true
.if empty(BSD)
NEED_LIBBSD=probably
.else
CFLAGS+=-I/usr/local/include
LDADD+=-L/usr/local/lib
.endif

.ifdef NEED_LIBBSD
CFLAGS+=-DNEED_LIBBSD
LDADD+=-lbsd
.endif

PREFIX?=/usr/local
BINDIR?=${PREFIX}/libexec
MANDIR?=${PREFIX}/man
WITHOUT_DEBUG_FILES=yes

.include <bsd.prog.mk>
