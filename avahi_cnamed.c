#include <err.h>
#include <netdb.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#ifdef NEED_LIBBSD
#	include <bsd/stdlib.h>
#endif

#include <sys/queue.h>

#include <avahi-client/client.h>
#include <avahi-client/publish.h>

#include <avahi-common/error.h>
#include <avahi-common/simple-watch.h>

#include "avahi_cnamed.h"

#define TTL 60
#define DEFAULT_ALIAS_FILE "/usr/local/etc/avahi_cnames"


/*
 * TODO
 *
 * reload on signal handling
 * 	can we exit the poll loop on a signal? probably not documented as safe
 *	so we should use something else as the loop. in the mean time just restart
 *	us.
 *
 * test restarting avahi
 *
 * add rc.d and upstart scripts
 */

// to be accesible to signal handlers :-(
static struct context *global_ctx;

static void usage(void) {

	fprintf(stderr, "usage: %s [-dhv] [-f alias_file]\n"
					"\n"
					"	-d	don't daemonise\n"
					"	-h	print this help message\n"
					"	-v	log information to stderr in addition to syslog (implies -d)\n"
					"	-f	read alias names from file instead of " DEFAULT_ALIAS_FILE "\n"
					"\n"
					"%s adds CNAMEs (aliases) for this host via Avahi (mDNS)\n",
		getprogname(), getprogname());
}

static void init_context(struct context *ctx) {

	SLIST_INIT(&(ctx->cnames));
	ctx->client = NULL;
	ctx->poll = NULL;
	ctx->file = DEFAULT_ALIAS_FILE;
	ctx->ec = 0;
	ctx->keep_running = true;

	global_ctx = ctx;
}

static void free_cnames(struct context *ctx) {

	// cnames
	struct cname_record *r;
	struct cname_record *r_tmp;

	SLIST_FOREACH_SAFE(r, &(ctx->cnames), next, r_tmp) {
		if (r->entry_group != NULL) {
			avahi_entry_group_free(r->entry_group);
		}
		if (r->name != NULL) {
			free(r->name);
		}
		SLIST_REMOVE(&(ctx->cnames), r, cname_record, next);
		free(r);
	}
}

static void handle_signal(int signo) {

	switch (signo) {
		case SIGHUP:
			// exit loop
			avahi_simple_poll_quit(global_ctx->poll);
			break;
		default:
			global_ctx->keep_running = false;
			avahi_simple_poll_quit(global_ctx->poll);
			break;
	}
}

static const char *name_for_group(AvahiEntryGroup *group, struct context *ctx) {
	struct cname_record *r;

	SLIST_FOREACH(r, &(ctx->cnames), next) {
		if (r->entry_group == group) {
			return r->name;
		}
	}

	return NULL;
}

static void group_callback(AvahiEntryGroup *group, AvahiEntryGroupState state, void *ctx) {
	const char *name = NULL;

	// look for the name associated with this group
	//
	// but don't bother looking for groups that won't yet be returned
	if (state != AVAHI_ENTRY_GROUP_UNCOMMITED) {
		name = name_for_group(group, (struct context *)ctx);
		if (name == NULL) {
			syslog(LOG_ERR, "group callback for unknown group");
		}
	}
	if (name == NULL) {
		name = "(unknown name)";
	}

	switch (state) {
		case AVAHI_ENTRY_GROUP_UNCOMMITED:
			// commit hasn't been called yet. but we will so ignore this
			break;
		case AVAHI_ENTRY_GROUP_REGISTERING:
			// still registering. nothing to do either
			syslog(LOG_DEBUG, "registering %s", name);
			break;
		case AVAHI_ENTRY_GROUP_ESTABLISHED:
			// registered successfully
			syslog(LOG_INFO, "registered %s", name);
			break;
		case AVAHI_ENTRY_GROUP_COLLISION:
			// already being claimed
			syslog(LOG_ERR, "name collision for %s. they won", name);
			break;
		case AVAHI_ENTRY_GROUP_FAILURE:
			// misc error, maybe invalid name or no network?
			syslog(LOG_ERR, "failed to advertise %s", name);
			break;
	}
}

static int read_alias_file(struct context *ctx) {
	FILE *fh;
	char line[NI_MAXHOST + 1];
	unsigned int line_count = 0;
	struct cname_record *rec;

	syslog(LOG_DEBUG, "processing file %s", ctx->file);

	if ((fh = fopen(ctx->file, "r")) == NULL) {
		return 0;
	}

	while ((fgets(line, sizeof(line), fh)) != NULL) {
		line_count++;
		// walk through the line, trimming leading and trailing space and
		// removing comments
		const char *start = NULL;
		char *end = NULL;
		bool seen_end = false;
		for (size_t i = 0; line[i] != '\0' && !seen_end; i++) {
			switch (line[i]) {
				case ' ':
				case '\t':
				case '\r':
					break;
				case '\n':
				case '#':
					// drop the \n or #
					line[i] = '\0';
					seen_end = true;
					break;
				default:
					if (start == NULL) {
						start = &(line[i]);
					}
					end = &(line[i]);
					break;
			}
		}
		if (start == NULL) {
			// nothing useful on this line
			continue;
		} else if (seen_end == false) {
			// line must have been too long or we would have found the \n
			syslog(LOG_ERR, "line %u too long: %.32s...", line_count, line);
			continue;
		} else {
			// we have to make sure that we trim whitespace. we know there is
			// at least one byte after end, though it may already be \0.
			*(end + 1) = '\0';
		}

		// we now should have a new record so create a struct to store it in
		// the list
		if ((rec = malloc(sizeof(struct cname_record))) == NULL) {
			syslog(LOG_ERR, "malloc failed (line %u). dropping %s: %m", line_count, line);
			continue;
		}
		// copy the name from our stack allocated buffer
		if ((rec->name = strdup(start)) == NULL) {
			syslog(LOG_ERR, "strdup failed (line %u). dropping %s: %m", line_count, line);
			free(rec);
			continue;
		}
		// set the group to NULL to remind us to create it later
		rec->entry_group = NULL;
		// and add it to the list
		SLIST_INSERT_HEAD(&(ctx->cnames), rec, next);
	}

	if (! feof(fh)) {
		// exited loop due to error
		syslog(LOG_ERR, "fgets: %m");
		return 0;
	}

	if (fclose(fh) != 0) {
		syslog(LOG_ERR, "fclose: %m");
		return 0;
	}

	// no guarentee we added anything to the list of course
	return 1;
}

static int hostname_to_rr(const char *h, char **rr) {
	const char *hw;
	char *rrw;
	uint8_t l;

	// allocate somewhere for the answer. we need 2 extra bytes, one for the
	// length of the first domain and one for the null
	*rr = malloc(strlen(h) + 2);
	if (*rr == NULL) {
		syslog(LOG_ERR, "malloc: %m");
		return 0;
	}

	hw = h;
	rrw = *rr;
	l = 0;
	for (;;) {
		if (*hw == '.' && l == 0) {
			syslog(LOG_ERR, "name %s has missing domain", h);
			return 0;
		} else if (*hw == '.' || *hw == '\0') {
			// end of a domain
			*rrw = l;
			memcpy(rrw + 1, (hw - l), l);
			rrw += 1 + l;

			l = 0;

			if (*hw == '\0') {
				// end of string
				break;
			}
		} else {
			l++;

			if (l > 63) {
				syslog(LOG_ERR, "name %s has too long domains", h);
				return 0;
			}
		}
		hw++;
	}

	return 1;
}

static void register_all(struct context *ctx) {
	int rc;

	// find our hostname
	const char *our_name;
	if ((our_name = avahi_client_get_host_name_fqdn(ctx->client)) == NULL) {
		syslog(LOG_ERR, "avahi_client_get_host_name_fqdn: %s", avahi_strerror(avahi_client_errno(ctx->client)));
		return;
	}

	// convert it to DNS RR format
	char *our_name_rr = NULL;
	if (! hostname_to_rr(our_name, &our_name_rr)) {
		syslog(LOG_ERR, "failed to foramt our own name");
		if (our_name_rr != NULL) {
			free(our_name_rr);
		}
		return;
	}
	
	// create and register each group
	struct cname_record *r;

	SLIST_FOREACH(r, &(ctx->cnames), next) {
		syslog(LOG_DEBUG, "processing name %s", r->name);
		// make sure we have an empty group
		if (r->entry_group == NULL) {
			syslog(LOG_DEBUG, "creating group for %s", r->name);
			if (! (r->entry_group = avahi_entry_group_new(ctx->client, group_callback, ctx))) {
				syslog(LOG_ERR, "avahi_entry_group_new: %s", avahi_strerror(avahi_client_errno(ctx->client)));
				continue;
			}
		} else if (! avahi_entry_group_is_empty(r->entry_group)) {
			syslog(LOG_DEBUG, "resetting group for %s", r->name);
			if ((rc = avahi_entry_group_reset(r->entry_group)) < 0) {
				syslog(LOG_ERR, "avahi_entry_group_reset: %s", avahi_strerror(rc));
				continue;
			}
		}

		// add our record to it
		if ((rc = avahi_entry_group_add_record(r->entry_group,
						AVAHI_IF_UNSPEC,
						AVAHI_PROTO_UNSPEC,
						AVAHI_PUBLISH_UNIQUE | AVAHI_PUBLISH_USE_MULTICAST,
						r->name,
						AVAHI_DNS_CLASS_IN,
						AVAHI_DNS_TYPE_CNAME,
						TTL,
						our_name_rr,
						strlen(our_name_rr) + 1	// include the null
						)) < 0) {
			syslog(LOG_ERR, "avahi_entry_group_add_record: %s", avahi_strerror(rc));
			continue;
		}

		// commit it
		if ((rc = avahi_entry_group_commit(r->entry_group)) < 0) {
			syslog(LOG_ERR, "avahi_entry_group_commit: %s", avahi_strerror(rc));
			continue;
		}

		syslog(LOG_INFO, "added cname for %s", r->name);
	}
}

static void client_callback(AvahiClient *client, AvahiClientState state, void *ctx) {

	((struct context *)ctx)->client = client;

	switch (state) {
		case AVAHI_CLIENT_S_RUNNING:
			// things are running so we should register our cnames
			register_all((struct context *)ctx);
			break;
		case AVAHI_CLIENT_FAILURE:
			syslog(LOG_CRIT, "avahi client error: %s", avahi_strerror(avahi_client_errno(client)));
			avahi_simple_poll_quit(((struct context *)ctx)->poll);
			break;
		case AVAHI_CLIENT_S_COLLISION:
			// someone else has registered our main name...i.e. a daemon
			// problem. we can't do anything so hope someone else fixes it
			break;
		case AVAHI_CLIENT_S_REGISTERING:
			// daemon is still registering - we just have to wait
			break;
		case AVAHI_CLIENT_CONNECTING:
			// server isn't running yet - we just have to wait
			break;
	}
}

int main(int argc, char *argv[]) {
	int rc, c, daemon_mode, verbose_mode;
	struct context ctx;

	// defaults
	daemon_mode = 1;
	verbose_mode = 0;

	// initialise context
	init_context(&ctx);

	// command line args
	while ((c = getopt(argc, argv, "df:hv")) != -1) {
		switch (c) {
			case 'f':
				ctx.file = optarg;
				break;
			case 'd':
				daemon_mode = 0;
				break;
			case 'v':
				verbose_mode++;
				daemon_mode = 0;
				break;
			case 'h':
				usage();
				exit(0);
			default:
				usage();
				exit(1);
		}
	}

	if (optind != argc) {
		usage();
		exit(1);
	}

	// start logging
	openlog("avahi_cnamed", (verbose_mode ? LOG_PERROR : 0), LOG_DAEMON);
	syslog(LOG_DEBUG, "starting");

	// enable signal handler
	signal(SIGHUP, handle_signal);
	signal(SIGINT, handle_signal);
	signal(SIGTERM, handle_signal);

	// fork into the background
	if (daemon_mode) {
		if (daemon(1, 0) != 0) {
			err(1, "daemon");
		}
	}

	while (ctx.keep_running) {
		
		// read the alias file
		if (! read_alias_file(&ctx)) {
			syslog(LOG_CRIT, "failed to read cname file");
			exit(1);
		}

		// create the poll object
		if ((ctx.poll = avahi_simple_poll_new()) == NULL) {
			syslog(LOG_CRIT, "avahi_simple_poll_new failed");
			ctx.ec = 1;
			break;
		}

		// create the client
		ctx.client = avahi_client_new(avahi_simple_poll_get(ctx.poll),
				AVAHI_CLIENT_NO_FAIL,
				client_callback,
				&ctx,
				&rc);
		if (ctx.client == NULL) {
			syslog(LOG_CRIT, "avahi_client_new: %s", avahi_strerror(rc));
			ctx.ec = 1;
			break;
		}

		// enter the event loop
		avahi_simple_poll_loop(ctx.poll);

		// we have exited so clean up, either for neatness or to be ready
		// to go around again
		
		syslog(LOG_DEBUG, "cleaning");
		free_cnames(&ctx);
		avahi_client_free(ctx.client);
		avahi_simple_poll_free(ctx.poll);
	}

	syslog(LOG_DEBUG, "finishing");
	closelog();

	return ctx.ec;
}
